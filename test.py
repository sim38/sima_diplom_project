import asyncio
import warnings

from asunc_mysql import create_pool

warnings.filterwarnings("ignore", category=DeprecationWarning)

host = 'localhost'
port = 3306
user = 'root'
password = 'toor'
db = 'database'
loop = asyncio.get_event_loop()

file_name = '/home/vova/PycharmProjects/sima_sql_generate/data.csv'
sep = '|'

file_model_name = 'qwerty12345.sav'
file_vector_name = 'qwerty12345_.sav'


async def test():
    pool = await create_pool(host=host, port=port, user=user, password=password, db=db, loop=loop)
    async with pool.acquire() as conn:
        async with conn.cursor() as cur:
            await cur.execute("")
            print(cur.description)
            r = await cur.fetchone()
    pool.close()
    await pool.wait_closed()


if __name__ == '__main__':
    loop.run_until_complete(test())
