import aiomysql

from asunc_mysql.Cursor import Cursor


class Connector:
    def __init__(self, pool: aiomysql.Pool):
        self._pool = pool
        self.conn: aiomysql.Connection

    async def __aenter__(self):
        self.conn = await self._pool.acquire()
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        try:
            self._pool.release(self.conn)
        finally:
            self.pool = None
            self.conn = None

    def cursor(self):
        return Cursor(self.conn)

    async def commit(self):
        await self.conn.commit()
