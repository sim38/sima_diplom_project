import pandas as pd
import joblib

from asunc_mysql.error import R2ScoreError, F1ScoreError
from asunc_mysql.const import file_vector_name, file_model_name

from sklearn.model_selection import train_test_split
from sklearn.feature_extraction.text import TfidfVectorizer
from sklearn.neural_network import MLPClassifier
from sklearn.metrics import r2_score, f1_score


def get_model(*args, **kwargs):
    return Model.create_model(*args, **kwargs)


class Model:
    def __init__(self, file_name: str = '', file_model_name_: str = file_model_name,
                 file_vector_name_: str = file_vector_name,
                 sep: str = '|', *args, **kwargs):
        self.file_model_name = file_model_name_
        self.file_vector_name = file_vector_name_
        if file_name:
            self.data = self._read_file(file_name, sep)
        self.vectorizer: TfidfVectorizer
        self.model: MLPClassifier

    @classmethod
    def create_model(cls, *args, **kwargs):
        model = cls(*args, **kwargs)

        if kwargs.get('file_vector_name'):
            model._load_vector(kwargs.get('file_vector_name'))
        else:
            model._fit_vectorizer()

        if kwargs.get('file_model_name'):
            model._load_models(kwargs.get('file_model_name'))
        else:
            model._fit()

        return model

    def __check_model(self, features, target):
        predict = self.model.predict(features)
        r2 = r2_score(target, predict)
        f1 = f1_score(target, predict)
        if r2 < 0.8:
            raise R2ScoreError(f'Низкое значение мертики R2 = {r2}')
        if f1 < 0.8:
            raise F1ScoreError(f'Низкое значение мертики F1 = {f1}')

    @staticmethod
    def _read_file(file_name, sep):
        df = pd.read_csv(file_name, sep=sep)
        df.columns = ['0', '1']
        return df

    def _fit(self):
        vectors = self.vectorization(self.data['1'].values)
        new_df = pd.DataFrame(vectors.toarray())
        features_train, features_test, target_train, target_test = train_test_split(
            new_df, self.data['0'], test_size=0.25
        )
        self.model = MLPClassifier(hidden_layer_sizes=(new_df.shape[1], 100, 10, 1), max_iter=100)
        self.model.fit(features_train, target_train)
        self.__check_model(features_test, target_test)
        self._save_model()

    def _fit_vectorizer(self):
        self.vectorizer = TfidfVectorizer()
        self.vectorizer.fit(self.data['1'].values)
        self._save_vector()

    @staticmethod
    def _split_data(features, target):
        return train_test_split(features, target, test_size=0.25)

    def vectorization(self, data):
        return self.vectorizer.transform(data)

    def predict(self, data: list):
        """"
            0 - не SQLi
            1 - SQLi
        """
        data = self.vectorization(data)
        return self.model.predict(data)

    def _save_model(self):
        joblib.dump(self.model, self.file_model_name)

    def _save_vector(self):
        joblib.dump(self.vectorizer, self.file_vector_name)

    def _load_models(self, file_name: str):
        self.model = joblib.load(file_name)

    def _load_vector(self, file_name: str):
        self.vectorizer = joblib.load(file_name)
