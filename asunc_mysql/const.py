import pathlib

BASE_DIR = pathlib.Path(__file__).parent.resolve()

file_model_name = f'{BASE_DIR}/qwerty12345.sav'
file_vector_name = f'{BASE_DIR}/qwerty12345_v.sav'
