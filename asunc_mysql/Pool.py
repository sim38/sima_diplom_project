import asyncio
import aiomysql

from asunc_mysql.Connection import Connector


async def create_pool(host, port, user, password, db, loop):
    pool = await aiomysql.create_pool(host=host, port=port, user=user, password=password, db=db, loop=loop)
    return Pool(pool)


class Pool:
    def __init__(self, pool: aiomysql.Pool,  *args, **kwargs):
        self.pool = pool

    def acquire(self) -> Connector:
        return Connector(self.pool)

    def close(self):
        self.pool.close()

    async def wait_closed(self):
        await self.pool.wait_closed()
