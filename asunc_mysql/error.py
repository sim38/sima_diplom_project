class R2ScoreError(Exception):
    ...


class F1ScoreError(Exception):
    ...


class SQLiError(Exception):
    ...
