import aiomysql

from asunc_mysql.error import SQLiError
from asunc_mysql.model import get_model
from asunc_mysql.const import *


class Cursor:
    def __init__(self, conn: aiomysql.Connection, *args, **kwargs):
        self._conn = conn
        self.cur: aiomysql.Cursor
        self.model = get_model(file_model_name=file_model_name, file_vector_name=file_vector_name)

    async def __aenter__(self):
        self.cur = await self._conn.cursor()
        return self

    async def __aexit__(self, exc_type, exc_val, exc_tb):
        try:
            await self.cur.close()
        finally:
            self._conn = None
            self.cur = None

    async def execute(self, execute: str):
        predict = self.model.predict([execute])
        if predict[0] == 1:
            raise SQLiError(f'Подозрение на SQLi "{execute}"')
        await self.cur.execute(execute)

    async def default_execute(self, execute: str):
        await self.cur.execute(execute)

    async def fetchone(self):
        return await self.cur.fetchone()

    @property
    def description(self):
        return self.cur.description
